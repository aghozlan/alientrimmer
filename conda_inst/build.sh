#!/bin/bash
BINDIR="$PREFIX/bin"
echo "$BINDIR"
cd $SRC_DIR/src/
javac AlienTrimmer.java
./JarMaker.sh
mv AlienTrimmer.jar $BINDIR/
echo -e "#!/bin/bash\nSCRIPTPATH=\$(dirname \"\${BASH_SOURCE[0]}\")\njava -jar \$SCRIPTPATH/AlienTrimmer.jar \$@" > $BINDIR/AlienTrimmer
chmod +x $BINDIR/AlienTrimmer